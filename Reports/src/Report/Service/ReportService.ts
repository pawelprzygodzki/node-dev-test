import {Injectable, Inject} from '@nestjs/common';
import {OrderMapper} from '../../Order/Service/OrderMapper';
import {BestSellerNotFoundException} from './exceptions/BestSellerNotFoundException';
import {Order} from '../../Order/Model/Order';
import {IBestBuyers, IBestSellers} from '../Model/IReports';
import {BestBuyersNotFoundException} from './exceptions/BestBuyersNotFoundException';

@Injectable()
export class ReportService {
    @Inject() orderMapper: OrderMapper;

    async getTheBestSellersForDate(date: Date): Promise<IBestSellers> {
        const orders = await this.orderMapper.fetchOrdersForDate(date);

        if (orders.length === 0) {
            throw new BestSellerNotFoundException();
        }

        const products = this.getListOfProducts(orders);

        const bestSeller = products
            .sort((product, nextProduct) => product.quantity - nextProduct.quantity)
            .pop();

        return {
            productName: bestSeller.name,
            quantity: bestSeller.quantity,
            totalPrice: bestSeller.quantity * bestSeller.price,
        };
    }

    private getListOfProducts(orders: Order[]) {
        const products = [];

        orders.forEach((order) => {
            order.products.forEach((product) => {
                const productIndex = products.findIndex((prod) => prod.id === product.id);

                if (productIndex === -1) {
                    product.quantity = 1;
                    products.push(product);
                } else {
                    products[productIndex].quantity++;
                }
            });
        });

        return products;
    }

    async getTheBestBuyersForDate(date: Date): Promise<IBestBuyers> {
        const orders = await this.orderMapper.fetchOrdersForDate(date);

        if (orders.length === 0) {
            throw new BestBuyersNotFoundException();
        }

        const customers = this.getListOfCustomers(orders);

        const bestBuyers = customers
            .sort((customer, nextCustomer) => customer.totalPrice - nextCustomer.totalPrice)
            .pop();

        return {
            customerName: `${bestBuyers.firstName} ${bestBuyers.lastName}`,
            totalPrice: bestBuyers.totalPrice,
        };
    }

    private getListOfCustomers(orders: Order[]) {
        const customers = [];

        orders.forEach((order) => {
            const customerIndex = customers.findIndex((customer) => customer.id === order.customer.id);

            const orderCustomer = order.customer;
            if (customerIndex === -1) {
                orderCustomer.totalPrice = order.getOrderSum();
                customers.push(orderCustomer);
            } else {
                orderCustomer.totalPrice += order.getOrderSum();
            }

        });

        return customers;
    }
}
