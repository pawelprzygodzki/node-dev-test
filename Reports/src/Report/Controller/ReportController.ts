import {BadRequestException, Controller, Get, Inject, NotFoundException, Param} from '@nestjs/common';
import {ReportService} from '../Service/ReportService';
import moment = require('moment');
import {BestSellerNotFoundException} from '../Service/exceptions/BestSellerNotFoundException';
import {BestBuyersNotFoundException} from '../Service/exceptions/BestBuyersNotFoundException';

@Controller()
export class ReportController {
    constructor(
        @Inject(ReportService) private readonly reportService: ReportService,
    ) {
    }

    @Get('/report/products/:date')
    async bestSellers(@Param('date') date: string) {
        try {
            const reportDate = moment(date);

            if (!reportDate.isValid()) {
                throw new BadRequestException('Invalid date');
            }

            return await this.reportService.getTheBestSellersForDate(reportDate.toDate());
        } catch (error) {
            if (error instanceof BestSellerNotFoundException) {
                throw new NotFoundException();
            }

            throw error;
        }
    }

    @Get('/report/customer/:date')
    async bestBuyers(@Param('date') date: Date) {
        try {
            const reportDate = moment(date);

            if (!reportDate.isValid()) {
                throw new BadRequestException('Invalid date');
            }

            return await this.reportService.getTheBestBuyersForDate(reportDate.toDate());
        } catch (error) {
            if (error instanceof BestBuyersNotFoundException) {
                throw new NotFoundException();
            }

            throw error;
        }
    }
}
