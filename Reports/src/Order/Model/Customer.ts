export class Customer {
    id: number;
    firstName: string;
    lastName: string;
    totalPrice: number;
}
