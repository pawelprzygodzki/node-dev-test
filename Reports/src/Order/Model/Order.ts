import {Customer} from './Customer';
import {Product} from './Product';

export class Order {
    number: string;
    customer: Customer;
    createdAt: string;
    products: Product[];

    getOrderSum() {
        return this.products
            .map((product) => product.price)
            .reduce((sum, nextPrice) => sum + nextPrice, 0);
    }
}
