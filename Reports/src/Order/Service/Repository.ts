import { Injectable } from '@nestjs/common';
import * as moment from 'moment';

/**
 * Data layer - mocked
 */
@Injectable()
export class Repository {
  fetchOrders(): Promise<any[]> {
    return new Promise(resolve => resolve(require('../Resources/Data/orders')));
  }

  async fetchOrdersForDate(date: Date) {
    const orders = await this.fetchOrders();

    return orders.filter((order) => moment(order.createdAt).isSame(moment(date)));
  }

  fetchProducts(): Promise<any[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/products')),
    );
  }

  fetchCustomers(): Promise<any[]> {
    return new Promise(resolve =>
      resolve(require('../Resources/Data/customers')),
    );
  }
}
