import {Injectable, Inject} from '@nestjs/common';
import {Repository} from './Repository';
import {plainToClass} from 'class-transformer';
import {Order} from '../Model/Order';

@Injectable()
export class OrderMapper {
    @Inject() repository: Repository;

    async fetchOrdersForDate(date: Date) {
        const [orders, products, customers] = await Promise.all([
            this.repository.fetchOrdersForDate(date),
            this.repository.fetchProducts(),
            this.repository.fetchCustomers(),
        ]);

        const productsMap = new Map();
        products.forEach((product) => {
            productsMap.set(product.id, product);
        });

        const customersMap = new Map();
        customers.forEach((customer) => {
            customersMap.set(customer.id, customer);
        });

        return orders.map((order) =>
            plainToClass(Order, {
                ...order,
                customer: customersMap.get(order.customer),
                products: order.products.map((productId) => productsMap.get(productId)),
            }),
        );
    }
}
