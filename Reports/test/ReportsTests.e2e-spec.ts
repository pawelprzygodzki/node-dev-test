import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { ReportModule } from './../src/Report/ReportModule';

describe('', () => {
  let app: INestApplication;

  beforeAll(async () => {
    const moduleFixture = await Test.createTestingModule({
      imports: [ReportModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(404);
  });

  it('/report/products/invalid_date (GET)', () => {
    return request(app.getHttpServer())
        .get('/report/products/invalid_date')
        .expect(400);
  });

  it('/report/products/date_with_best_seller (GET)', () => {
    return request(app.getHttpServer())
        .get('/report/products/2019-08-07')
        .expect(200, {
          productName: 'Black sport shoes',
          quantity: 2,
          totalPrice: 220,
        });
  });

  it('/report/products/date_without_best_seller (GET)', () => {
    return request(app.getHttpServer())
        .get('/report/products/2019-08-01')
        .expect(404);
  });

  it('/report/customer/date_with_best_buyer (GET)', () => {
    return request(app.getHttpServer())
        .get('/report/customer/2019-08-07')
        .expect(200, {
          customerName: 'John Doe',
          totalPrice: 135.75,
        });
  });

  it('/report/customer/date_without_best_buyers (GET)', () => {
    return request(app.getHttpServer())
        .get('/report/customer/2019-08-01')
        .expect(404);
  });
});
