export class ASort { // Bubble sort
    static sort(numberArray: number[]) {
        const result = [...numberArray];
        let swapped = false;
        do {
            swapped = false;
            result.forEach((current, i) => {
                if (current > result[i + 1]) {
                    const temp = current;

                    result[i] = result[i + 1];
                    result[i + 1] = temp;
                    swapped = true
                }
            })
        } while (swapped);

        return result;
    }
}
