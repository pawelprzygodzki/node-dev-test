export class BSort { // Quick sort
    static sort(numberArray: number[]) {
        if (numberArray.length < 2) {
            return numberArray;
        }

        const chosenIndex = numberArray.length - 1;
        const element = numberArray[chosenIndex];

        const firstPart = [];
        const secondPart = [];
        for (let i = 0; i < chosenIndex; i++) {
            const temp = numberArray[i];

            temp < element ? firstPart.push(temp) : secondPart.push(temp)
        }

        return [...BSort.sort(firstPart), element, ...BSort.sort(secondPart)];
    }
}
