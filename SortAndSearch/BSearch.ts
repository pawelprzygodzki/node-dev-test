export class BSearch {
    private static instance;
    private numberOfOperations = 0;

    private constructor() {
    }

    static getInstance() {
        if (!BSearch.instance) {
            BSearch.instance = new BSearch();
        }
        return BSearch.instance;
    }

    getNumberOfOperations() {
        return this.numberOfOperations;
    }

    binarySearch(numberArray: number[], target: number) {
        this.numberOfOperations = 0;
        let startIndex = 0;
        let lastIndex = numberArray.length - 1;

        while (startIndex <= lastIndex) {
            this.numberOfOperations++;

            let middleIndex = Math.floor((startIndex + lastIndex) / 2);

            if (target === numberArray[middleIndex]) {
                return middleIndex;
            }
            if (target > numberArray[middleIndex]) {
                startIndex = middleIndex + 1;
            }

            if (target < numberArray[middleIndex]) {
                lastIndex = middleIndex - 1;
            }
        }

        return -1;
    }
}
